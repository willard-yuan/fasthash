Fast Supervised Hashing with Decision Trees for High-Dimensional Data
=====================================================================

- This is the code for the paper [Fast Supervised Hashing with Decision Trees for High-Dimensional Data", IEEE Conf. Computer Vision and Pattern Recognition, 2014 (CVPR2014)](https://bitbucket.org/chhshen/fasthash/src/ab55808c06ef132850e69d197165c64ed85ce61b/CVPR2014.pdf?at=master).

- [Download this repository](https://bitbucket.org/chhshen/fasthash/downloads)

- If you use this code in your research, please cite our paper:

```
 @inproceedings{CVPR2014Lin,
   author    = "Guosheng Lin and  Chunhua Shen and  Qinfeng Shi and Anton {van den Hengel} and David Suter",
   title     = "Fast Supervised Hashing with Decision Trees for High-Dimensional Data",
   booktitle = "Proc. IEEE Conf. Computer Vision and Pattern Recognition",
   year      = "2014",
 }
```




## Install

Several toolboxes are required for running this code. For convenience, they are included in the folder: `/libs`
 and pre-compiled in Linux. These toolboxes are as follows:

- GraphCut is required for binary code inference, we use the implementation at <http://www.csd.uwo.ca/~olga/OldCode.html>, and the Matlab wrapper written by Brian Fulkerson.

- For fast decision tree training, we use the implementation from [Piotr's Image & Video Matlab Toolbox](http://vision.ucsd.edu/~pdollar/toolbox/doc/).


The implementation of two-step hashing (TSH) is also included here. 
Different types of loss functions and hash functions (e.g., linear SVM) are included in this code. 
Please refer to the description of TSH for details: <https://bitbucket.org/guosheng/two-step-hashing>.


The file `demo.m` shows that how to use the code.
Kindly note that the setting of tree depth should be adapted to datasets.



## Copyright

Copyright (c) Guosheng Lin, Chunhua Shen. 2014.

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.